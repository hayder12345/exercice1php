
<?php

//procédure boucle_while
function boucle_while()
{
    $i=1;
    echo $i;
    while ($i<=50){
        echo('" * ",$i');
        $i++;
    }
}

//procédure boucle_for
function boucle_for()
{
    echo(1);
    for ($i=2; $i<=50; $i++){
        echo('" * ",$i');
    }
}

// appel des procedures
boucle_while();
boucle_for()
?>

<form method="POST" action="boucle_while()">
  <div>
	<input type="submit" value="test_boucle_while" />
  </div>
</form>

<form method="POST" action="boucle_for()">
  <div>
	<input type="submit" value="test_boucle_for" />
  </div>
</form>